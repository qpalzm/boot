package ru.test.boot.serivce;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.test.boot.model.User;
import ru.test.boot.repository.UsersRepository;

@Service
public class UsersService {

    @Autowired
    private UsersRepository repository;

    @Transactional
    public void createUser(User user) {
        repository.save(user);
    }

    @Transactional
    public List<User> findAll() {
        return repository.findAll();
    }

    @Transactional
    public void deleteUser(User user){
        repository.delete(user);
    }

    @Transactional
    public void updateUser(User user){
        repository.saveAndFlush(user);
    }

    @Transactional
    public User findById(Long userId) {
        return repository.findById(userId).orElse(null);
    }

    @Transactional
    public List<User> findAllByName(String name) {
        return repository.findAllByName(name);
    }

    @Transactional
    public List<User> findWhereNameStartsFromSmith(String str) {
        return repository.findWhereNameStartsFrom(str);
    }
}
