package ru.test.boot.serivce;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.test.boot.model.Role;
import ru.test.boot.repository.RoleRepository;

@Service
public class RoleService {

    @Autowired
    private RoleRepository repository;

    @Transactional
    public void addRole(Role role) {
        repository.save(role);
    }

    @Transactional
    public List<Role> findAll() {
        return repository.findAll();
    }

    @Transactional
    public Role findById(Long id) {
        return repository.findById(id).orElse(null);
    }

}
