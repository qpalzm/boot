package ru.test.boot.serivce;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.test.boot.model.Group;
import ru.test.boot.repository.GroupRepository;

@Service
public class GroupService {

    @Autowired
    private GroupRepository repository;

    @Transactional
    public void addGroup(Group Group) {
        repository.save(Group);
    }

    @Transactional
    public List<Group> findAll() {
        return repository.findAll();
    }

    @Transactional
    public Group findById(Long id) {
        return repository.findById(id).orElse(null);
    }
}
