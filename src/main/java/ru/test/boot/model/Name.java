package ru.test.boot.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class Name implements Serializable {

    private String name;

    private String description;

    public Name(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Name() {
    }
}
