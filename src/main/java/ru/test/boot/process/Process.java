package ru.test.boot.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.test.boot.model.Group;
import ru.test.boot.model.Name;
import ru.test.boot.model.Role;
import ru.test.boot.model.User;
import ru.test.boot.serivce.GroupService;
import ru.test.boot.serivce.RoleService;
import ru.test.boot.serivce.UsersService;

@Component
public class Process {

    @Autowired
    private UsersService usersService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private RoleService roleService;

   // @EventListener(ApplicationReadyEvent.class)
    private void run(){
        System.out.println("Start method run");
        Role roleAdmin = new Role();
        roleAdmin.setName(new Name("admin", "role for admin"));
        roleAdmin.setLevel(0);
        roleService.addRole(roleAdmin);

        Role roleUser = new Role();
        roleUser.setName(new Name("user", "role for user"));
        roleUser.setLevel(99);
        roleService.addRole(roleUser);


        Group generalGroup = new Group();
        generalGroup.setName(new Name("general", "general group"));
        groupService.addGroup(generalGroup);


        User admin = new User();
        admin.setRole(roleAdmin);
        admin.setGroup(generalGroup);
        admin.setName(new Name("admin", "account for admin"));
        usersService.createUser(admin);

        User user = new User();
        user.setGroup(generalGroup);
        user.setRole(roleUser);
        user.setName(new Name("test", "account for test"));
        usersService.createUser(user);

        System.out.println("End method run");

    }
}
