package ru.test.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.test.boot.model.Group;

public interface GroupRepository extends JpaRepository<Group, Long> {

}
