package ru.test.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.test.boot.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
