package ru.test.boot.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.test.boot.model.User;

public interface UsersRepository extends JpaRepository<User, Long> {

    List<User> findAllByName(String name);//просто правильное название метода даст возможность
    //избежать запросов на SQL

    @Query(value = "select * from users where name like :str", nativeQuery = true)
        //если и этого мало - можно написать запрос на чистом SQL и все это будет работать
    List<User> findWhereNameStartsFrom(@Param("str") String str);

}
