package ru.test.boot.controller;

import java.util.List;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.test.boot.model.User;
import ru.test.boot.serivce.UsersService;

@RestController
@RequestMapping("/api")
public class UsersController {

    @Autowired
    private UsersService  service;

    @GetMapping("/users")
    List<User> getAllUsers(){
        return service.findAll();
    }

    @PostMapping("/createUsers")
    ResponseEntity<Void> createUser(@RequestBody User user) {
        service.createUser(user);
        return ResponseEntity.ok().build();
    }

    @RequestMapping("/getUser/{id}")
    public User getUser(@PathVariable("id") String id){
        return  service.findById(Long.valueOf(id));
    }

    @GetMapping("/deleteUser/{id}")
    public void deleteUser(@PathVariable("id") String id){
        User user = service.findById(Long.valueOf(id));
        service.deleteUser(user);
    }



}
