package ru.test.boot.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.test.boot.model.Group;
import ru.test.boot.model.Name;
import ru.test.boot.model.Role;
import ru.test.boot.model.User;
import ru.test.boot.repository.UsersRepository;

import static org.junit.Assert.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class UsersControllerTest {

    @Autowired
    private UsersRepository repository;
    @Autowired
    private UsersController controller;

    @Test
    public void testGetUser(){
        User user = new User(1l, new Name("test", "test"), new Role(), new Group());
        ResponseEntity<Void> entity = controller.createUser(user);
        assertEquals(entity.getStatusCode(), HttpStatus.OK);
    }

}